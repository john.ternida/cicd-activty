FROM node:14.19

WORKDIR /app

COPY package.json .
COPY package-lock.json .

RUN npm install

COPY . .

EXPOSE 4245

CMD ["npm", "start"]